import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QRPassComponent } from './qrpass.component';

describe('QRPassComponent', () => {
  let component: QRPassComponent;
  let fixture: ComponentFixture<QRPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QRPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QRPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
