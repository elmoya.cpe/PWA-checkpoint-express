import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { RegistrationStatusComponent } from './registration-status.component';

@NgModule({
  declarations: [RegistrationStatusComponent],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [RegistrationStatusComponent]
})
export class RegistrationStatusModule {}
