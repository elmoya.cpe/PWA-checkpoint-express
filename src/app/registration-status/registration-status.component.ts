import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

export enum RegistrationStatus {
  Pending,
  Approved,
  Denied
}

@Component({
  selector: 'app-registration-status',
  templateUrl: './registration-status.component.html',
  styleUrls: ['./registration-status.component.scss']
})
export class RegistrationStatusComponent implements OnInit {

  public status: RegistrationStatus;

  public statuses = RegistrationStatus;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      const id = +params.id;
      // mapping of ids is for mocking the status only, remove this during integration
      if (id === 1) {
        this.status = RegistrationStatus.Pending;
      } else if (id === 2) {
        this.status = RegistrationStatus.Approved;
      } else {
        this.status = RegistrationStatus.Denied;
      }
    });
  }

  public ngOnInit(): void {
  }

}
