import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationStatusComponent } from './registration-status/registration-status.component';
import { ScannerComponent } from './scanner/scanner.component';
import { QRPassComponent } from './qrpass/qrpass.component';


const routes: Routes = [
  { path: 'scanner', component: ScannerComponent, data: { title: 'Scanner' } },
  { path: 'request', component: QRPassComponent, data: { title: 'Request QR Pass' } },
  { path: 'status/:id', component: RegistrationStatusComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
